//
//  ViewController.m
//  autolayout
//
//  Created by Daniel Phillips on 02/06/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import "ViewController.h"

#define USE_CONTENT_MODE 0 // toggle using content mode scale fit. If you don't use this, the explicit height constraint of the image view will be set each time the image changes.

@interface ViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)changePhotoPressed:(id)sender;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)changePhotoPressed:(id)sender {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.delegate = self;
    
    [self presentViewController:imagePicker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    self.imageView.image = image;
    
    if(USE_CONTENT_MODE){
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }else{
        // calculate the correct height of the image given the current width of the image view.
        CGFloat multiplier = (CGRectGetWidth(self.imageView.bounds) / image.size.width);
        
        // update the height constraint with the new known constant (height)
        [self.imageViewHeightConstraint setConstant:multiplier * image.size.height];
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
